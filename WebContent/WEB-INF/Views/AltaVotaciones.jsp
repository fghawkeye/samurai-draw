<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Alta de Votaciones</title>
<%@include file="HeaderAdmin.jsp"%>

<!-- cdn for modernizr, if you haven't included it already -->
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
<!-- polyfiller file to detect and load polyfills -->
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>

</head>
<body>

<div class="container">
	
	<form action="AltaVotacion.html" method="post">
		  <div class="form-group">
		    <label for="FechaLimite">Fecha limite</label>
		    <input type="date" class="form-control" id="FechaLimite" name="FechaLimite" placeholder="Fecha limite">
		  </div>
		  
		  <div class="form-group">
			  <table id="listadoPost" class="table table-hover">
				<tr class="header">
					<th style="width: 30%;">ID</th>
					<th style="width: 30%;">Nombre</th>
					<th style="width: 30%;">Imagen</th>
					<th style="width: 10%;"></th>
				</tr>
				
				<c:forEach items="${posts}" step="1" var="post">
					<tr>
						<td>
							<c:out value="${post.idPost}"/>
						</td>
						<td>
							<c:out value="${post.titulo}"/>
						</td>
						<td>
							<img src="${path}${post.imagen}" class="avatar">
						</td>
						<td>
							<button class="class="btn btn-success" type=button onclick="agregarPost(${post.idPost})">
								Seleccionar
							</button>
											
						</td>
					</tr>
				</c:forEach>
				
			</table>
			
		  </div>
		  
		  <input type="hidden" id="listadoId" name="listadoId" value="">
		  
		  <button type="submit" class="btn btn-primary">Crear Votacion</button>
	</form>
		

</div>
	
	<script>
		webshims.setOptions('waitReady', false);
	  	webshims.setOptions('forms-ext', {types: 'date'});
	  	webshims.polyfill('forms forms-ext');
	
		function agregarPost(idPost) {
			var actual = document.getElementById("listadoId").value;
			if(actual !== null && actual !== ''){
				document.getElementById("listadoId").value = actual+","+idPost;
			}
			else{
				document.getElementById("listadoId").value = idPost;
			}	
			alert('Se agrego el post a la votacion');
		}
	</script>
	

</body>
</html>