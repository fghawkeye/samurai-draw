<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="entidad.Usuario,utility.TipoUsuario,org.springframework.context.ApplicationContext,org.springframework.web.context.support.WebApplicationContextUtils"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Crear post</title>

<%
	ApplicationContext ctx = WebApplicationContextUtils
			.getRequiredWebApplicationContext(config.getServletContext());
	String tipo = ((Usuario) ctx.getBean("_userSessionBean")).getTipo();
	if (tipo.equals(TipoUsuario.Administrador.toString())) {
%>
<%@include file="HeaderAdmin.jsp"%>
<%
	} else if (tipo.equals(TipoUsuario.Usuario.toString())) {
%>
<%@include file="HeaderUsuario.jsp"%>
<%
	}
%>

</head>
<body>

	<form action="AltaPost.html" enctype="multipart/form-data" method="post">
		<div class="form-group">
			<label for="Titulo">Titulo</label> <input type="text"
				class="form-control" name="Titulo" id="Titulo">
		</div>
		<div class="form-group">
			<label for=Categorias>Categoria</label>
			<select name="Categoria" id="Categorias">
				<c:forEach items="${categorias}" var="categoria">
					<option value="${categoria.getIdCategoria()}">${categoria.getNombre()}</option>
				</c:forEach>
			</select>
		</div>
		<div class="form-group">
			<label for="Descripcion">Descripcion</label> <input type="text"
				class="form-control" id="Descripcion" name="Descripcion">
		</div>
		<div class="form-group">
			<input type="file" id="file" name="image" />
			<output id="uploadedImage"></output>
		</div>

		<button type="submit" class="btn btn-default">Crear Post</button>
	</form>

<script>
  function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
          span.innerHTML = ['<img class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('uploadedImage').insertBefore(span, null);
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  }

  document.getElementById('file').addEventListener('change', handleFileSelect, false);
</script>

</body>
</html>