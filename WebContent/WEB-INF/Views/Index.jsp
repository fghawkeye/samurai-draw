<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="entidad.Usuario,utility.TipoUsuario,org.springframework.context.ApplicationContext,org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Samurai Draw</title>

<%
	ApplicationContext ctx = WebApplicationContextUtils
			.getRequiredWebApplicationContext(config.getServletContext());
	String tipo = ((Usuario) ctx.getBean("_userSessionBean")).getTipo();
	if (tipo.equals(TipoUsuario.Administrador.toString())) {
%>
<%@include file="HeaderAdmin.jsp"%>
<%
	} else if (tipo.equals(TipoUsuario.Usuario.toString())) {
%>
<%@include file="HeaderUsuario.jsp"%>
<%
	} else {
%>
<%@include file="HeaderAnonimo.jsp"%>
<%
	}
%>

<style>
/* Three image containers (use 25% for four, and 50% for two, etc) */
.column {
	float: left;
	width: 33.33%;
	padding: 5px;
}

/* Clear floats after image containers */
.row::after {
	content: "";
	clear: both;
	display: table;
}

.row .column img {
	height: 200px;
	border: 1px solid #000;
	margin: 10px 5px 0 0;
}
</style>

</head>
<body>


	<div class="container">
		<div class="row">
			<c:forEach items="${posts}" step="1" var="img">
				<div class="column">

					<c:out value="${img.titulo}" />
					<br />
					<c:set var="idUrl">
						<c:url value="AbrirPost.html">
							<c:param name="id" value="${img.idPost}" />
						</c:url>
					</c:set>
					<a href="${idUrl}"> <img src="${img.imagen}"
						style="width: 100%">
					</a>


				</div>
			</c:forEach>
		</div>

	</div>

</body>
</html>