<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page
	import="entidad.Usuario,utility.TipoUsuario,org.springframework.context.ApplicationContext,org.springframework.web.context.support.WebApplicationContextUtils"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ver post</title>

<%
	ApplicationContext ctx = WebApplicationContextUtils
			.getRequiredWebApplicationContext(config.getServletContext());
	String tipo = ((Usuario) ctx.getBean("_userSessionBean")).getTipo();
	if (tipo.equals(TipoUsuario.Administrador.toString())) {
%>
<%@include file="HeaderAdmin.jsp"%>
<%
	} else if (tipo.equals(TipoUsuario.Usuario.toString())) {
%>
<%@include file="HeaderUsuario.jsp"%>
<%
	} else {
%>
<%@include file="HeaderAnonimo.jsp"%>
<%} %>

</head>
<body>

<div class="container">
	 <div class="row">
		  <div class="col">
		    <img src="${post.imagen}" width="450px" height="500px">
		  </div>
		  <div class="col" style="margin-left:70px">
			  	 <div class="form-row">
				   <div class="col">
			      		<h2>Titulo: ${post.titulo}</h2>
			    	</div>
			      </div>
			      <div class="form-row">
			    	<div class="col">
			      		<h3>Categoria: ${post.categoria.nombre}</h3>
			    	</div>
				</div>
				<div class="form-row">
					<div class="col">
						${post.descripcion}
					</div>
				</div>
				<div class="form-row" style="margin-top:20px">
					<div class="col">
						<h3>Artista: ${post.usuario.userName}</h3>
					</div>
					<div class="col">
						<img src="${post.usuario.imagenPerfil}" class="avatar">
					</div>	
				</div>
		  </div>
	</div>
	
	<div class="row" style="margin-top:20px">
	 	<div class="col">
			 	<c:set var="idUrl">
					<c:url value="AltaComentario.html">
					<c:param name="id" value="${post.idPost}"/>
					</c:url>
				</c:set>
			<form action="${idUrl}" method="post">
				<div class="form-group">
					<label for="Comentario">Agrega un comentario</label> 
				     <textarea class="form-control" id="Comentario" name="Comentario"></textarea>
				</div>
				<button type="submit" class="btn btn-default">Agregar Comentario</button>
			</form>
		</div>
	</div>
	
	<div class="row" style="margin-top:50px">
		<div class="col">
			<table>
				<tr>
					<th>
						<h2>Comentarios</h2>
					</th>
				</tr>
				<c:forEach items="${comentarios}" step="1" var="comentario">
					<tr>
							<td>
								<h3><c:out value="${comentario.usuario.userName}"/></h3>
								<br>
								<c:out value="${comentario.comentario}"/>
							</td>
					</tr>
				</c:forEach>	
			</table>
		</div>
	</div>

</div>

</body>
</html>