<%@page pageEncoding="UTF-8"%>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1">

<!--CSS-->
<style type="text/css">
	<%@ include file="/WEB-INF/Views/css/Style.css" %>
</style>

<!--Bootstrap-->
<!--CSS-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<!--JS<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>-->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>

 
<!--FONT AWESOME-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" 
integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

    
<body>
<div class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a href="Inicio.jsp"><h1 class="text-white">Samurai Draw</h1></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  
  <div class="collapse navbar-collapse justify-content-between" id="navbarSupportedContent">
  <ul class="navbar-nav">
  	<li class="nav-item" style="margin-left: 10px;">
			  <div class="dropdown m-1">
		    <button class="btn btn-secondary dropdown-toggle" type="button" id="Alumnos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Alumnos
		    </button>
		    <div class="dropdown-menu" aria-labelledby="Alumnos">
		      <a class="dropdown-item" href="RegistrarAlumnos.jsp">Registrar Alumnos</a>
		      <a class="dropdown-item" href="ServletAlumnosABM">Gestionar Alumnos</a>
		    </div>
		  </div>  
	  </li>
	  <li>
		 <div class="dropdown m-1">
		    <button class="btn btn-secondary dropdown-toggle" type="button" id="Profesores" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Profesores
		    </button>
		    <div class="dropdown-menu" aria-labelledby="Profesores">
		      <a class="dropdown-item" href="RegistrarProfesores.jsp">Registrar Profesores</a>
		      <a class="dropdown-item" href="ServletProfesoresABM">Gestionar Profesores</a>
		    </div>
		  </div> 
	  </li>
	  
	  	<li class="nav-item">
	  		<a href="ServletCursosABM" class="btn btn-secondary m-1">Cursos</a>
	  	</li>
  </ul>
  <ul class="navbar-nav">
	  <li class="nav-item">
		  		<div class="dropdown m-1">
		  		<button class="btn btn-secondary dropdown-toggle" type="button" id="User" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">${_userSessionBean.userName}
				    </button>
				    <div class="dropdown-menu-right dropdown-menu" aria-labelledby="User">
				      <a class="dropdown-item" href="CambiarContraseña.jsp">Cambiar Contraseña</a>
				      <a class="dropdown-item" href="CerrarSesion.html">Cerrar Sesion</a>
				    </div>
		  		</div> 
	  	</li>
	  
  </ul>
  </div>  
</div>

</body>
</html>