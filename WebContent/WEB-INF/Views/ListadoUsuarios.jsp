<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Listado de Usuarios</title>

<%@include file="HeaderAdmin.jsp"%>

</head>
<body>

	<div class="container">
		<input type="text" class="form-control" id="filtro" onkeyup="filtrar()" placeholder="Nombre de usuario">

		<table id="listadoUsuarios" class="table table-hover">
			<tr class="header">
				<th style="width: 60%;">Nombre de Usuario</th>
				<th style="width: 40%;">Estado</th>
				<th></th>
			</tr>
			
			<c:forEach items="${usuarios}" step="1" var="usuario">
				<tr>
					<td>
						<c:out value="${usuario.userName}"/>
					</td>
					<td>
						<c:out value="${usuario.estado}"/>
					</td>
					<td>
						<c:set var="idUrl">
					      	<c:url value="BajaUsuario.html">
								<c:param name="id" value="${usuario.idUsuario}"/>
							</c:url>
					     </c:set>
						<a href="${idUrl}">
							<i class="fas fa-trash-alt"></i>
						</a>
					</td>
				</tr>
			</c:forEach>
			
		</table>

	</div>

	<script>
		function filtrar() {
			var input, filter, table, tr, td, i, txtValue;
			input = document.getElementById("filtro");
			filter = input.value.toUpperCase();
			table = document.getElementById("listadoUsuarios");
			tr = table.getElementsByTagName("tr");

			// Loop through all table rows, and hide those who don't match the search query
			for (i = 0; i < tr.length; i++) {
				td = tr[i].getElementsByTagName("td")[0];
				if (td) {
					txtValue = td.textContent || td.innerText;
					if (txtValue.toUpperCase().indexOf(filter) > -1) {
						tr[i].style.display = "";
					} else {
						tr[i].style.display = "none";
					}
				}
			}
		}
	</script>
</body>
</html>