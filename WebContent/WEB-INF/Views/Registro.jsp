<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registro</title>

<style type="text/css">
<%@ include file="/WEB-INF/Views/css/Style.css" %>
</style>

</head>
<body style="background: linear-gradient(to left, #161b15, #8FB794);">

<div class="form">
    <form class="login-form" method="post" action="Registro.html">
      <input type="text" name="Usuario" placeholder="Usuario"/>
      <input type="password" name="Contrasenia" placeholder="Contraseņa"/>
      <button>Registrarse</button>
      <p class="message">Ya estas registrado? <a href="AbrirLogin.html">ingresa aqui</a></p>
    </form>
  </div>

</body>
</html>