<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page
	import="entidad.Usuario,utility.TipoUsuario,org.springframework.context.ApplicationContext,org.springframework.web.context.support.WebApplicationContextUtils"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ver Perfil</title>

<%
	ApplicationContext ctx = WebApplicationContextUtils
			.getRequiredWebApplicationContext(config.getServletContext());
	String tipo = ((Usuario) ctx.getBean("_userSessionBean")).getTipo();
	if (tipo.equals(TipoUsuario.Administrador.toString())) {
%>
<%@include file="HeaderAdmin.jsp"%>
<%
	} else if (tipo.equals(TipoUsuario.Usuario.toString())) {
%>
<%@include file="HeaderUsuario.jsp"%>
<%
	}
%>

</head>
<body>

<div class="container">
	
 <form action="ModificarPerfil.html" enctype="multipart/form-data" method="post">
 		<div class="form-group">
			<h2>${usuario.userName}</h2>
			Se unio el ${usuario.fechaCreacion}
		</div>
		<div class="form-group">
			<label for="Descripcion">Descripcion</label> 
		     <textarea class="form-control" id="Descripcion" name="Descripcion">${usuario.descripcion}</textarea>
		</div>
		<div class="form-group">
			<img src="${usuario.imagenPerfil}">
		</div>
		<div class="form-group">
			<label for="image">Cambiar foto de perfil</label>
			<input type="file" id="file" name="image" />
		</div>

		<button type="submit" class="btn btn-default">Modificar Perfil</button>		
  </form>
</div>


</body>
</html>