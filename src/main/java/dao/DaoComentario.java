package dao;

import java.util.List;

import entidad.Comentario;

public interface DaoComentario {
	public void AddComentario(Comentario _c);

	public List<Comentario> GetAllComentarios();

	public Comentario GetComentario(int id);

	public void DeleteComentario(int id);

	public void ModifyComentario(Comentario _c);

	public List<Comentario> GetAllComentariosByPost(int id);

}
