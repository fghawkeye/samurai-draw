package dao;

import java.util.List;

import entidad.Votacion;

public interface DaoVotacion {

	public int AddVotacion(Votacion _v);

	public List<Votacion> GetAllVotaciones();

	public Votacion GetVotacion(int id);

	public void DeleteVotacion(int id);

	public void ModifyVotacion(Votacion _v);

	public Votacion ObtenerVotacionActiva();

}
