package dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import entidad.Votacion;

public class DaoVotacionImpl implements DaoVotacion {

	private HibernateTemplate hibernateTemplate = null;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public int AddVotacion(Votacion _v) {
		List<Votacion> v = GetAllVotaciones();
		if (v.size() > 1)
			return 0;
		else {
			return (int) this.hibernateTemplate.save(_v);
		}

	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<Votacion> GetAllVotaciones() {
		return (List<Votacion>) this.hibernateTemplate.loadAll(Votacion.class);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Votacion GetVotacion(int id) {
		return this.hibernateTemplate.get(Votacion.class, id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void DeleteVotacion(int id) {
		Votacion v = new Votacion();
		v.setIdVotacion(id);
		this.hibernateTemplate.delete(v);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void ModifyVotacion(Votacion _v) {
		this.hibernateTemplate.update(_v);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Votacion ObtenerVotacionActiva() {
		String query = "from Votacion v where v.estado=" + 1;
		return (Votacion) this.hibernateTemplate.find(query);
	}

}
