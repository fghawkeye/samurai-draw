package dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import entidad.VotosPost;

public class DaoVotosPostImpl implements DaoVotosPost {

	private HibernateTemplate hibernateTemplate = null;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void AddVotosPost(VotosPost _v) {
		this.hibernateTemplate.save(_v);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<VotosPost> GetAllVotosPost(int idvotacion) {
		String query = "from VotosPost v where v.votacion.idVotacion=" + idvotacion;
		return (List<VotosPost>) this.hibernateTemplate.find(query);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public VotosPost GetVotosPost(int idvotacion, int idusuario) {
		String query = "from VotosPost v where v.votacion.idVotacion=" + idvotacion + " and m.usuario.idUsuario="
				+ idusuario;
		return (VotosPost) this.hibernateTemplate.find(query).get(0);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void DeleteVotosPost(VotosPost _v) {
		this.hibernateTemplate.delete(_v);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void ModifyVotosPost(VotosPost _v) {
		this.hibernateTemplate.update(_v);
	}

	/*
	 * public int PuntajeMasAlto() { SessionFactory sessionFactory = null; Session
	 * session = null; int max = 0; try { sessionFactory = Conexion.Open(); session
	 * = sessionFactory.openSession(); max = (Integer)
	 * session.createQuery("SELECT MAX(v.Puntaje) FROM VotosPost v").uniqueResult();
	 * } catch (Exception ex) { ex.printStackTrace(); } finally { if (session !=
	 * null) session.close(); if (sessionFactory != null) sessionFactory.close(); }
	 * return max; }
	 */
}
