package dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import entidad.Usuario;

public class DaoUsuarioImpl implements DaoUsuario {

	private HibernateTemplate hibernateTemplate = null;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public boolean AddUsuario(Usuario _u) {
		String query = "from Usuario u where u.userName='" + _u.getUserName() + "'";
		if (this.hibernateTemplate.find(query).isEmpty()) {
			this.hibernateTemplate.save(_u);
			return true;
		} else
			return false;

	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<Usuario> GetAllUsuarios() {
		return (List<Usuario>) this.hibernateTemplate.loadAll(Usuario.class);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Usuario GetUsuario(int id) {
		return this.hibernateTemplate.get(Usuario.class, id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void DeleteUsuario(int id) {
		Usuario u = new Usuario();
		u.setIdUsuario(id);
		this.hibernateTemplate.delete(u);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void ModifyUsuario(Usuario _u) {
		this.hibernateTemplate.update(_u);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Usuario VerificarLogin(String username, String password) {
		String query = "from Usuario u where u.userName='" + username + "' and u.pass='" + password + "'";
		return (Usuario) this.hibernateTemplate.find(query).get(0);
	}

}
