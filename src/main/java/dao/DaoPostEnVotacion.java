package dao;

import java.util.List;

import entidad.PostEnVotacion;

public interface DaoPostEnVotacion {

	public void AddPostEnVotacion(PostEnVotacion _p);

	public List<PostEnVotacion> GetAllPostEnVotacion(int idvotacion);

	public PostEnVotacion GetPostEnVotacion(int idvotacion, int idpost);

	public void DeletePostEnVotacion(PostEnVotacion _p);

	public void ModifyPostEnVotacion(PostEnVotacion _p);

}
