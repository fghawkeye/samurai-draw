package dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import entidad.Comentario;

public class DaoComentarioImpl implements DaoComentario {

	private HibernateTemplate hibernateTemplate = null;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void AddComentario(Comentario _c) {
		this.hibernateTemplate.save(_c);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<Comentario> GetAllComentarios() {
		return (List<Comentario>) this.hibernateTemplate.loadAll(Comentario.class);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Comentario GetComentario(int id) {
		return this.hibernateTemplate.get(Comentario.class, id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void DeleteComentario(int id) {
		Comentario c = new Comentario();
		c.setIdComentario(id);
		this.hibernateTemplate.delete(c);

	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void ModifyComentario(Comentario _c) {
		this.hibernateTemplate.update(_c);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<Comentario> GetAllComentariosByPost(int id) {
		String query = "from Comentario c where c.post.idPost=" + id;
		return (List<Comentario>) this.hibernateTemplate.find(query);
	}

}
