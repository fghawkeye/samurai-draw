package dao;

import java.util.List;

import entidad.Usuario;

public interface DaoUsuario {

	public boolean AddUsuario(Usuario _u);

	public List<Usuario> GetAllUsuarios();

	public Usuario GetUsuario(int id);

	public void DeleteUsuario(int id);

	public void ModifyUsuario(Usuario _u);

	public Usuario VerificarLogin(String username, String password);
}
