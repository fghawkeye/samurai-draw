package dao;

import java.util.List;

import entidad.Categoria;

public interface DaoCategoria {
	public void AddCategoria(Categoria _c);

	public List<Categoria> GetAllCategorias();

	public Categoria GetCategoria(int id);

	public void DeleteCategoria(int id);
}
