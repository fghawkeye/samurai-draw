package dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import entidad.Categoria;

public class DaoCategoriaImpl implements DaoCategoria {
	public DaoCategoriaImpl() {
	}

	private HibernateTemplate hibernateTemplate = null;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	/// Funcion que agrega una categoria recibiendo un objeto categoria
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void AddCategoria(Categoria _c) {
		this.hibernateTemplate.save(_c);
	}

	/// Funcion que permite obtener todas las categorias existentes
	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<Categoria> GetAllCategorias() {
		return (List<Categoria>) this.hibernateTemplate.loadAll(Categoria.class);
	}

	/// Funcion que permite obtener todas las categorias existentes
	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Categoria GetCategoria(int id) {
		return this.hibernateTemplate.get(Categoria.class, id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void DeleteCategoria(int id) {
		Categoria c = new Categoria();
		c.setIdCategoria(id);
		this.hibernateTemplate.delete(c);
	}

}
