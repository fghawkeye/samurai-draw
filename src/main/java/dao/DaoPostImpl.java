package dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import entidad.Post;

public class DaoPostImpl implements DaoPost {

	private HibernateTemplate hibernateTemplate = null;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void AddPost(Post _p) {
		this.hibernateTemplate.save(_p);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<Post> GetAllPosts() {
		return (List<Post>) this.hibernateTemplate.loadAll(Post.class);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Post GetPost(int id) {
		return this.hibernateTemplate.get(Post.class, id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void DeletePost(int id) {
		Post p = new Post();
		p.setIdPost(id);
		this.hibernateTemplate.delete(p);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void ModifyPost(Post _p) {
		this.hibernateTemplate.update(_p);
	}

}
