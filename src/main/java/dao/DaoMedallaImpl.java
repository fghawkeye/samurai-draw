package dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import entidad.Medalla;

public class DaoMedallaImpl implements DaoMedalla {

	private HibernateTemplate hibernateTemplate = null;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void AddMedalla(Medalla _m) {
		this.hibernateTemplate.save(_m);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<Medalla> GetAllMedallas(int idpost) {
		String query = "from Medalla m where m.post.idPost=" + idpost;
		return (List<Medalla>) this.hibernateTemplate.find(query);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Medalla GetMedalla(int numMedalla, int idpost, int idvotacion) {
		// return this.hibernateTemplate.get(Medalla.class, numMedalla);
		String query = "from Medalla m where m.numeroMedalla=" + numMedalla + " and m.post.idPost=" + idpost
				+ " and m.votacion.idVotacion=" + idvotacion;
		return (Medalla) this.hibernateTemplate.find(query).get(0);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void DeleteMedalla(Medalla m) {
		this.hibernateTemplate.delete(m);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void ModifyMedalla(Medalla _m) {
		this.hibernateTemplate.update(_m);
	}

}
