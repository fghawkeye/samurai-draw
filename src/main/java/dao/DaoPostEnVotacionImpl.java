package dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import entidad.PostEnVotacion;

public class DaoPostEnVotacionImpl implements DaoPostEnVotacion {

	private HibernateTemplate hibernateTemplate = null;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void AddPostEnVotacion(PostEnVotacion _p) {
		this.hibernateTemplate.save(_p);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<PostEnVotacion> GetAllPostEnVotacion(int idvotacion) {
		String query = "from PostEnVotacion p where p.votacion.idVotacion=" + idvotacion;
		return (List<PostEnVotacion>) this.hibernateTemplate.find(query);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public PostEnVotacion GetPostEnVotacion(int idvotacion, int idpost) {
		String query = "from PostEnVotacion p where p.votacion.idVotacion=" + idvotacion + " and p.post.idPost="
				+ idpost;
		return (PostEnVotacion) this.hibernateTemplate.find(query).get(0);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void DeletePostEnVotacion(PostEnVotacion _p) {
		this.hibernateTemplate.delete(_p);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void ModifyPostEnVotacion(PostEnVotacion _p) {
		this.hibernateTemplate.update(_p);
	}

}
