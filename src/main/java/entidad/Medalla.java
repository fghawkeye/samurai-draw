package entidad;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tblmedallas")
@IdClass(value = MedallaPkCompuesta.class)
public class Medalla implements Serializable {

	private static final long serialVersionUID = 1L;

	public Medalla(int numeroMedalla, Post post, Votacion votacion, Date fechaObtencion, String descripcion) {
		this.numeroMedalla = numeroMedalla;
		this.post = post;
		this.votacion = votacion;
		this.fechaObtencion = fechaObtencion;
		this.descripcion = descripcion;
	}

	@Id
	@Column(name = "NumeroMedalla")
	int numeroMedalla;

	@Id
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idPost")
	Post post;

	@Id
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idVotacion")
	Votacion votacion;

	@Column(name = "FechaObtencion")
	Date fechaObtencion;

	@Column(name = "Descripcion")
	String descripcion;

	public int getNumeroMedalla() {
		return numeroMedalla;
	}

	public void setNumeroMedalla(int numeroMedalla) {
		this.numeroMedalla = numeroMedalla;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public Votacion getVotacion() {
		return votacion;
	}

	public void setVotacion(Votacion votacion) {
		this.votacion = votacion;
	}

	public Date getFechaObtencion() {
		return fechaObtencion;
	}

	public void setFechaObtencion(Date fechaObtencion) {
		this.fechaObtencion = fechaObtencion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Medalla() {
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		result = prime * result + ((fechaObtencion == null) ? 0 : fechaObtencion.hashCode());
		result = prime * result + numeroMedalla;
		result = prime * result + ((post == null) ? 0 : post.hashCode());
		result = prime * result + ((votacion == null) ? 0 : votacion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Medalla other = (Medalla) obj;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		if (fechaObtencion == null) {
			if (other.fechaObtencion != null)
				return false;
		} else if (!fechaObtencion.equals(other.fechaObtencion))
			return false;
		if (numeroMedalla != other.numeroMedalla)
			return false;
		if (post == null) {
			if (other.post != null)
				return false;
		} else if (!post.equals(other.post))
			return false;
		if (votacion == null) {
			if (other.votacion != null)
				return false;
		} else if (!votacion.equals(other.votacion))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Medalla [numeroMedalla=" + numeroMedalla + ", post=" + post + ", votacion=" + votacion
				+ ", fechaObtencion=" + fechaObtencion + ", descripcion=" + descripcion + "]";
	}

}

class MedallaPkCompuesta implements Serializable {

	private static final long serialVersionUID = 1L;

	public int numeroMedalla;
	public Post post;
	public Votacion votacion;

	public MedallaPkCompuesta(int numeroMedalla, Post post, Votacion votacion) {
		this.numeroMedalla = numeroMedalla;
		this.post = post;
		this.votacion = votacion;
	}

	public MedallaPkCompuesta() {
	}

	public int getNumeroMedalla() {
		return numeroMedalla;
	}

	public void setNumeroMedalla(int numeroMedalla) {
		this.numeroMedalla = numeroMedalla;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public Votacion getVotacion() {
		return votacion;
	}

	public void setVotacion(Votacion votacion) {
		this.votacion = votacion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + numeroMedalla;
		result = prime * result + ((post == null) ? 0 : post.hashCode());
		result = prime * result + ((votacion == null) ? 0 : votacion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MedallaPkCompuesta other = (MedallaPkCompuesta) obj;
		if (numeroMedalla != other.numeroMedalla)
			return false;
		if (post == null) {
			if (other.post != null)
				return false;
		} else if (!post.equals(other.post))
			return false;
		if (votacion == null) {
			if (other.votacion != null)
				return false;
		} else if (!votacion.equals(other.votacion))
			return false;
		return true;
	}

}
