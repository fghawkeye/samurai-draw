package entidad;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tblvotaciones")
public class Votacion implements Serializable {

	private static final long serialVersionUID = 1L;

	public Votacion(Post postGanador, Post postSegundo, Post postTercero, Date fechaLimite, int estado) {
		super();
		this.postGanador = postGanador;
		this.postSegundo = postSegundo;
		this.postTercero = postTercero;
		this.fechaLimite = fechaLimite;
		this.estado = estado;
	}

	@Id
	@Column(name = "idVotacion")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int idVotacion;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idPostGanador")
	Post postGanador;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idPostSegundo")
	Post postSegundo;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idPostTercero")
	Post postTercero;

	@Column(name = "FechaLimite")
	Date fechaLimite;

	@Column(name = "Estado")
	int estado;

	public int getIdVotacion() {
		return idVotacion;
	}

	public void setIdVotacion(int idVotacion) {
		this.idVotacion = idVotacion;
	}

	public Post getPostGanador() {
		return postGanador;
	}

	public void setPostGanador(Post postGanador) {
		this.postGanador = postGanador;
	}

	public Post getPostSegundo() {
		return postSegundo;
	}

	public void setPostSegundo(Post postSegundo) {
		this.postSegundo = postSegundo;
	}

	public Post getPostTercero() {
		return postTercero;
	}

	public void setPostTercero(Post postTercero) {
		this.postTercero = postTercero;
	}

	public Date getFechaLimite() {
		return fechaLimite;
	}

	public void setFechaLimite(Date fechaLimite) {
		this.fechaLimite = fechaLimite;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Votacion() {
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + estado;
		result = prime * result + ((fechaLimite == null) ? 0 : fechaLimite.hashCode());
		result = prime * result + idVotacion;
		result = prime * result + ((postGanador == null) ? 0 : postGanador.hashCode());
		result = prime * result + ((postSegundo == null) ? 0 : postSegundo.hashCode());
		result = prime * result + ((postTercero == null) ? 0 : postTercero.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Votacion other = (Votacion) obj;
		if (estado != other.estado)
			return false;
		if (fechaLimite == null) {
			if (other.fechaLimite != null)
				return false;
		} else if (!fechaLimite.equals(other.fechaLimite))
			return false;
		if (idVotacion != other.idVotacion)
			return false;
		if (postGanador == null) {
			if (other.postGanador != null)
				return false;
		} else if (!postGanador.equals(other.postGanador))
			return false;
		if (postSegundo == null) {
			if (other.postSegundo != null)
				return false;
		} else if (!postSegundo.equals(other.postSegundo))
			return false;
		if (postTercero == null) {
			if (other.postTercero != null)
				return false;
		} else if (!postTercero.equals(other.postTercero))
			return false;
		return true;
	}

}
