package entidad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tblvotosxpost")
@IdClass(value = VotosPostPkCompuesta.class)
public class VotosPost implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idVotacion")
	Votacion votacion;

	@Id
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idUsuario")
	Usuario usuario;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idPost")
	Post post;

	@Column(name = "Puntaje")
	int puntaje;

	public VotosPost(Votacion votacion, Usuario usuario, Post post, int puntaje) {
		super();
		this.votacion = votacion;
		this.usuario = usuario;
		this.post = post;
		this.puntaje = puntaje;
	}

	public Votacion getVotacion() {
		return votacion;
	}

	public void setVotacion(Votacion votacion) {
		this.votacion = votacion;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public int getPuntaje() {
		return puntaje;
	}

	public void setPuntaje(int puntaje) {
		this.puntaje = puntaje;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public VotosPost() {
	}
}

class VotosPostPkCompuesta implements Serializable {

	private static final long serialVersionUID = 1L;

	public Votacion votacion;
	public Usuario usuario;

	public VotosPostPkCompuesta(Votacion votacion, Usuario usuario) {
		super();
		this.votacion = votacion;
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		result = prime * result + ((votacion == null) ? 0 : votacion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VotosPostPkCompuesta other = (VotosPostPkCompuesta) obj;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		if (votacion == null) {
			if (other.votacion != null)
				return false;
		} else if (!votacion.equals(other.votacion))
			return false;
		return true;
	}

	public Votacion getVotacion() {
		return votacion;
	}

	public void setVotacion(Votacion votacion) {
		this.votacion = votacion;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public VotosPostPkCompuesta() {
	}
}
