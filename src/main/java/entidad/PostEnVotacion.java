package entidad;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tblpostenvotacion")
@IdClass(value = PostEnVotacionPkCompuesta.class)
public class PostEnVotacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idVotacion")
	Votacion votacion;

	@Id
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idPost")
	Post post;

	public Votacion getVotacion() {
		return votacion;
	}

	public void setVotacion(Votacion votacion) {
		this.votacion = votacion;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public PostEnVotacion() {

	}

}

class PostEnVotacionPkCompuesta implements Serializable {

	private static final long serialVersionUID = 1L;

	public Votacion votacion;
	public Post post;

	public PostEnVotacionPkCompuesta(Votacion votacion, Post post) {
		super();
		this.votacion = votacion;
		this.post = post;
	}

	public PostEnVotacionPkCompuesta() {

	}

	public Votacion getVotacion() {
		return votacion;
	}

	public void setVotacion(Votacion votacion) {
		this.votacion = votacion;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((post == null) ? 0 : post.hashCode());
		result = prime * result + ((votacion == null) ? 0 : votacion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PostEnVotacionPkCompuesta other = (PostEnVotacionPkCompuesta) obj;
		if (post == null) {
			if (other.post != null)
				return false;
		} else if (!post.equals(other.post))
			return false;
		if (votacion == null) {
			if (other.votacion != null)
				return false;
		} else if (!votacion.equals(other.votacion))
			return false;
		return true;
	}

}
