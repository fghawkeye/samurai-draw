package entidad;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tblcomentarios")
public class Comentario implements Serializable {

	private static final long serialVersionUID = 1L;

	public Comentario(Usuario usuario, Post post, String comentario, Date fechaCreacion) {
		super();
		this.usuario = usuario;
		this.post = post;
		this.comentario = comentario;
		FechaCreacion = fechaCreacion;
	}

	@Id
	@Column(name = "idComentario")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int idComentario;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idUsuario")
	Usuario usuario;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idPost")
	Post post;

	@Column(name = "Comentario")
	String comentario;

	@Column(name = "FechaCreacion")
	Date FechaCreacion;

	public int getIdComentario() {
		return idComentario;
	}

	public void setIdComentario(int idComentario) {
		this.idComentario = idComentario;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Date getFechaCreacion() {
		return FechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		FechaCreacion = fechaCreacion;
	}

	public Comentario() {
	}
}
