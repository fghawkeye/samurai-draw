package entidad;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tblusuarios")
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	public Usuario(int id, String username, String pass, String tipo, Date fechaCreacion, String imagenPerfil,
			String descripcion, int estado) {
		this.idUsuario = id;
		this.userName = username;
		this.pass = pass;
		this.tipo = tipo;
		this.fechaCreacion = fechaCreacion;
		this.imagenPerfil = imagenPerfil;
		this.descripcion = descripcion;
		this.estado = estado;
	}

	public Usuario(String username, String pass, String tipo, Date fechaCreacion, String imagenPerfil,
			String descripcion, int estado) {
		this.userName = username;
		this.pass = pass;
		this.tipo = tipo;
		this.fechaCreacion = fechaCreacion;
		this.imagenPerfil = imagenPerfil;
		this.descripcion = descripcion;
		this.estado = estado;
	}

	@Id
	@Column(name = "idUsuario")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int idUsuario;

	@Column(name = "Username")
	String userName;

	@Column(name = "Password")
	String pass;

	@Column(name = "Tipo")
	String tipo;

	@Column(name = "FechaCreacion")
	Date fechaCreacion;

	@Column(name = "ImagenPerfil")
	String imagenPerfil;

	@Column(name = "Descripcion")
	String descripcion;

	@Column(name = "Estado")
	Integer estado;

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getImagenPerfil() {
		return imagenPerfil;
	}

	public void setImagenPerfil(String imagenPerfil) {
		this.imagenPerfil = imagenPerfil;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Usuario() {
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		result = prime * result + estado;
		result = prime * result + ((fechaCreacion == null) ? 0 : fechaCreacion.hashCode());
		result = prime * result + idUsuario;
		result = prime * result + ((imagenPerfil == null) ? 0 : imagenPerfil.hashCode());
		result = prime * result + ((pass == null) ? 0 : pass.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		if (estado != other.estado)
			return false;
		if (fechaCreacion == null) {
			if (other.fechaCreacion != null)
				return false;
		} else if (!fechaCreacion.equals(other.fechaCreacion))
			return false;
		if (idUsuario != other.idUsuario)
			return false;
		if (imagenPerfil == null) {
			if (other.imagenPerfil != null)
				return false;
		} else if (!imagenPerfil.equals(other.imagenPerfil))
			return false;
		if (pass == null) {
			if (other.pass != null)
				return false;
		} else if (!pass.equals(other.pass))
			return false;
		if (tipo == null) {
			if (other.tipo != null)
				return false;
		} else if (!tipo.equals(other.tipo))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

}
