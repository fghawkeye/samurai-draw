package controllers;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import entidad.Usuario;
import service.ImagenServicio;
import service.UsuarioServicio;
import utility.FuncionesComunes;
import utility.TipoUsuario;

@Controller
public class UserController {
	@Autowired
	public UsuarioServicio _userService;

	@Autowired
	private Usuario _userSessionBean;
	@Autowired
	private FuncionesComunes _funcionesComunesBean;
	@Autowired
	private ImagenServicio _imagenService;

	public void init(ServletConfig config) {
		ApplicationContext ctx = WebApplicationContextUtils
				.getRequiredWebApplicationContext(config.getServletContext());

		this._userService = (UsuarioServicio) ctx.getBean("serviceUsuarioBean");
	}

	@RequestMapping("AbrirLogin.html")
	public ModelAndView AbrirLogin() {
		ModelAndView MV = new ModelAndView();
		MV.setViewName("Login");
		return MV;
	}

	@RequestMapping("Login.html")
	public ModelAndView Login(String Usuario, String Contrasenia) {
		ModelAndView MV = new ModelAndView();
		try {
			Usuario u = _userService.VerificarLogin(Usuario, Contrasenia);
			if (u != null) {
				if (u.getEstado() == 1) {
					_userSessionBean.setTipo(u.getTipo());
					_userSessionBean.setIdUsuario(u.getIdUsuario());
					_userSessionBean.setUserName(u.getUserName());
					_userSessionBean.setImagenPerfil(u.getImagenPerfil());
					MV.setViewName("forward:/Index.html");
				} else {
					MV.addObject("msj", "Cuenta inhabilitada");
					MV.setViewName("Login");
				}

			} else {
				MV.addObject("msj", "Usuario y/o Contraseña invalidos");
				MV.setViewName("Login");
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			MV.addObject("msj", "Hubo un error al tratar de logearse.");
			MV.setViewName("Login");
		}
		return MV;
	}

	@RequestMapping("AbrirRegistro.html")
	public ModelAndView AbrirRegistro() {
		ModelAndView MV = new ModelAndView();
		MV.setViewName("Registro");
		return MV;
	}

	@RequestMapping("Registro.html")
	public ModelAndView Registro(String Usuario, String Contrasenia) {
		ModelAndView MV = new ModelAndView();
		try {
			Usuario u = new Usuario();
			u.setUserName(Usuario);
			u.setPass(Contrasenia);
			u.setFechaCreacion(new Date());
			u.setEstado(1);
			u.setTipo(TipoUsuario.Usuario.toString());
			if (_userService.AddUsuario(u)) {
				_userSessionBean.setTipo(u.getTipo());
				_userSessionBean.setIdUsuario(u.getIdUsuario());
				_userSessionBean.setUserName(u.getUserName());
				_userSessionBean.setImagenPerfil(u.getImagenPerfil());
				MV.setViewName("forward:/Index.html");
			} else {
				MV.addObject("msj", "Ya existe un usuario con el mismo nombre, elija otro diferente.");
				MV.setViewName("Registro");
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			MV.addObject("msj", "Hubo un error al tratar de registrarse.");
			MV.setViewName("Registro");
		}
		return MV;
	}

	@RequestMapping("CerrarSesion.html")
	public ModelAndView CerrarSesion() {
		ModelAndView MV = new ModelAndView();
		_userSessionBean.setTipo(null);
		_userSessionBean.setIdUsuario(0);
		_userSessionBean.setUserName(null);
		_userSessionBean.setImagenPerfil(null);
		MV.setViewName("Login");
		return MV;
	}

	@RequestMapping("AbrirPerfil.html")
	public ModelAndView AbrirPerfil(int id) {
		ModelAndView MV = new ModelAndView();
		Usuario u = _userService.GetUsuario(id);
		MV.addObject("usuario", u);
		// MV.addObject("path", _funcionesComunesBean.ObtenerPath());
		MV.setViewName("VerPerfil");
		return MV;
	}

	@RequestMapping("ModificarPerfil.html")
	public ModelAndView ModificarPerfil(String Descripcion, MultipartFile image) {
		ModelAndView MV = new ModelAndView();
		if (image.getSize() > 0) {
			Map uploadedImage = _imagenService.SubirImagen(image, "profiles");
			if (!uploadedImage.isEmpty()) {
				Usuario u = _userService.GetUsuario(_userSessionBean.getIdUsuario());
				u.setDescripcion(Descripcion);
				u.setImagenPerfil(uploadedImage.get("url").toString());
				_userService.ModifyUsuario(u);
				MV.setViewName("forward:/AbrirPerfil.html?id=" + _userSessionBean.getIdUsuario());
			} else {
				MV.setViewName("forward:/Index.html");
			}
		} else if (!Descripcion.isEmpty()) {
			Usuario u = _userService.GetUsuario(_userSessionBean.getIdUsuario());
			u.setDescripcion(Descripcion);
			_userService.ModifyUsuario(u);
			MV.setViewName("forward:/AbrirPerfil.html?id=" + _userSessionBean.getIdUsuario());
		}

		return MV;
	}

	@RequestMapping("AbrirListadoUsuarios.html")
	public ModelAndView AbrirListadoUsuarios() {
		ModelAndView MV = new ModelAndView();
		List<Usuario> usuarios = _userService.GetAllUsuarios();
		MV.addObject("usuarios", usuarios);
		MV.setViewName("ListadoUsuarios");
		return MV;
	}

	@RequestMapping("BajaUsuario.html")
	public ModelAndView BajaUsuario(int id) {
		ModelAndView MV = new ModelAndView();
		Usuario u = _userService.GetUsuario(id);
		u.setEstado(0);
		_userService.ModifyUsuario(u);
		MV.setViewName("forward:/AbrirListadoUsuarios.html");
		return MV;
	}

	/*
	 * private boolean CargarImagen(MultipartFile image) { try { byte[] bytes =
	 * image.getBytes(); Path path = Paths.get(_funcionesComunesBean.ObtenerPath() +
	 * "perfil_" + image.getOriginalFilename()); Files.write(path, bytes); return
	 * true; } catch (Exception ex) { System.out.println(ex); return false; } }
	 */

}
