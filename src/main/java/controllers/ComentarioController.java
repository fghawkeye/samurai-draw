package controllers;

import java.util.Date;

import javax.servlet.ServletConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import entidad.Comentario;
import entidad.Usuario;
import service.ComentarioServicio;
import service.PostServicio;
import service.UsuarioServicio;

@Controller
public class ComentarioController {

	@Autowired
	public PostServicio _postService;
	@Autowired
	public ComentarioServicio _comentarioService;
	@Autowired
	public UsuarioServicio _usuarioService;
	@Autowired
	private Usuario _userSessionBean;

	public void init(ServletConfig config) {
		ApplicationContext ctx = WebApplicationContextUtils
				.getRequiredWebApplicationContext(config.getServletContext());

		this._postService = (PostServicio) ctx.getBean("servicePostBean");
		this._comentarioService = (ComentarioServicio) ctx.getBean("serviceComentarioBean");
		this._usuarioService = (UsuarioServicio) ctx.getBean("serviceUsuarioBean");
	}

	@RequestMapping("AltaComentario.html")
	public ModelAndView AltaComentario(String Comentario, int id) {
		System.out.println(Comentario);
		System.out.println(id);
		ModelAndView MV = new ModelAndView();
		Comentario c = new Comentario();
		c.setComentario(Comentario);
		c.setUsuario(_usuarioService.GetUsuario(_userSessionBean.getIdUsuario()));
		c.setPost(_postService.GetPost(id));
		c.setFechaCreacion(new Date());
		_comentarioService.AddComentario(c);
		MV.setViewName("forward:/AbrirPost.html?id=" + id);
		return MV;
	}

}
