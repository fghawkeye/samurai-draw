package controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import entidad.Post;
import entidad.PostEnVotacion;
import entidad.Votacion;
import service.PostEnVotacionServicio;
import service.PostServicio;
import service.VotacionServicio;
import service.VotosPostServicio;
import utility.FuncionesComunes;

@Controller
public class VotacionController {

	@Autowired
	public PostServicio _postService;
	@Autowired
	public VotacionServicio _votacionService;
	@Autowired
	public VotosPostServicio _votosPostService;
	@Autowired
	public PostEnVotacionServicio _postEnVotacionService;
	@Autowired
	private FuncionesComunes _funcionesComunesBean;

	public void init(ServletConfig config) {
		ApplicationContext ctx = WebApplicationContextUtils
				.getRequiredWebApplicationContext(config.getServletContext());

		this._postService = (PostServicio) ctx.getBean("servicePostBean");
		this._votacionService = (VotacionServicio) ctx.getBean("serviceVotacionBean");
		this._votosPostService = (VotosPostServicio) ctx.getBean("serviceVotosPostBean");
		this._postEnVotacionService = (PostEnVotacionServicio) ctx.getBean("servicePostEnVotacionBean");
	}

	@RequestMapping("AbrirAltaVotacion.html")
	public ModelAndView AbrirAltaVotacion() {
		ModelAndView MV = new ModelAndView();
		List<Post> posts = _postService.GetAllPosts();
		MV.addObject("posts", posts);
		MV.addObject("path", _funcionesComunesBean.ObtenerPath());
		MV.setViewName("AltaVotaciones");
		return MV;
	}

	@RequestMapping("AltaVotacion.html")
	public ModelAndView AltaVotacion(String FechaLimite, String listadoId) throws ParseException {
		ModelAndView MV = new ModelAndView();
		System.out.println(FechaLimite);
		System.out.println(listadoId);
		Votacion v = new Votacion();
		v.setEstado(1);
		Date date = new SimpleDateFormat("yyyy-MM-dd").parse(FechaLimite);
		v.setFechaLimite(date);
		int idVotacion = _votacionService.AddVotacion(v);
		if (idVotacion > 0) {
			AgregarPostEnVotacion(listadoId, idVotacion);
			MV.setViewName("forward:/AbrirListadoVotaciones.html");
		} else {
			MV.setViewName("forward:/AbrirAltaVotacion.html");
		}
		return MV;
	}

	@RequestMapping("AbrirListadoVotaciones.html")
	public ModelAndView AbrirListadoVotaciones() {
		ModelAndView MV = new ModelAndView();
		Votacion v = _votacionService.ObtenerVotacionActiva();
		List<PostEnVotacion> p = _postEnVotacionService.GetAllPostEnVotacion(v.getIdVotacion());
		MV.setViewName("ListadoVotaciones");
		return MV;
	}

	private void AgregarPostEnVotacion(String listadoId, int idVotacion) {
		List<String> listado = Arrays.asList(listadoId.split(","));
		PostEnVotacion p = new PostEnVotacion();
		p.setVotacion(_votacionService.GetVotacion(idVotacion));
		for (String id : listado) {
			p.setPost(_postService.GetPost(Integer.parseInt(id)));
			_postEnVotacionService.AddPostEnVotacion(p);
		}
	}

}
