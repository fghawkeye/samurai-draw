package controllers;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import entidad.Categoria;
import entidad.Post;
import entidad.Usuario;
import service.CategoriaServicio;
import service.ComentarioServicio;
import service.ImagenServicio;
import service.PostServicio;
import service.UsuarioServicio;
import utility.FuncionesComunes;

@Controller
public class PostController {

	@Autowired
	public PostServicio _postService;
	@Autowired
	public CategoriaServicio _categoriaService;
	@Autowired
	public ComentarioServicio _comentarioService;
	@Autowired
	private Usuario _userSessionBean;
	@Autowired
	public UsuarioServicio _usuarioService;
	@Autowired
	private FuncionesComunes _funcionesComunesBean;
	@Autowired
	private ImagenServicio _imagenService;

	public void init(ServletConfig config) {
		ApplicationContext ctx = WebApplicationContextUtils
				.getRequiredWebApplicationContext(config.getServletContext());

		this._postService = (PostServicio) ctx.getBean("servicePostBean");
		this._categoriaService = (CategoriaServicio) ctx.getBean("serviceCategoriaBean");
	}

	@RequestMapping("Index.html")
	public ModelAndView redireccion() {
		ModelAndView MV = new ModelAndView();
		List<Post> posts = _postService.GetAllPosts();

		MV.addObject("posts", posts);
		MV.setViewName("Index");
		return MV;
	}

	@RequestMapping("CrearPost.html")
	public ModelAndView CrearPost() {
		ModelAndView MV = new ModelAndView();
		List<Categoria> categorias = _categoriaService.GetAllCategorias();
		MV.addObject("categorias", categorias);
		MV.setViewName("CrearPost");
		return MV;
	}

	@RequestMapping("AltaPost.html")
	public ModelAndView AltaPost(String Titulo, Integer Categoria, String Descripcion, MultipartFile image) {
		ModelAndView MV = new ModelAndView();
		try {
			Map uploadedImage = _imagenService.SubirImagen(image, "posts");
			if (!uploadedImage.isEmpty()) {
				Post p = new Post();
				p.setTitulo(Titulo);
				p.setDescripcion(Descripcion);
				// p.setImagen(image.getOriginalFilename());
				p.setImagen(uploadedImage.get("url").toString());
				p.setCategoria(_categoriaService.GetCategoria(Categoria));
				p.setEstado(1);
				p.setFechaCreacion(new Date());
				p.setUsuario(_usuarioService.GetUsuario(_userSessionBean.getIdUsuario()));
				_postService.AddPost(p);
				MV.setViewName("forward:/Index.html");
			} else {
				MV.setViewName("CrearPost");
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			MV.setViewName("CrearPost");
		}

		return MV;
	}

	@RequestMapping("AbrirPost.html")
	public ModelAndView AbrirPost(int id) {
		ModelAndView MV = new ModelAndView();
		Post post = _postService.GetPost(id);
		MV.addObject("post", post);
		// MV.addObject("path", _funcionesComunesBean.ObtenerPath());
		MV.addObject("comentarios", _comentarioService.GetAllComentariosByPost(id));
		MV.setViewName("VerPost");
		return MV;
	}

	/*
	 * private boolean CargarImagen(MultipartFile image) { try { byte[] bytes =
	 * image.getBytes(); Path path = Paths.get(_funcionesComunesBean.ObtenerPath() +
	 * image.getOriginalFilename()); // System.out.println(path.toString());
	 * Files.write(path, bytes); return true; } catch (Exception ex) {
	 * System.out.println(ex); return false; } }
	 */

}
