package service;

import java.util.List;

import dao.DaoVotacion;
import entidad.Votacion;

public class VotacionServicioImpl implements VotacionServicio {

	private DaoVotacion dataAccess = null;

	public void setDataAccess(DaoVotacion dataAccess) {
		this.dataAccess = dataAccess;
	}

	@Override
	public int AddVotacion(Votacion _v) {
		return dataAccess.AddVotacion(_v);
	}

	@Override
	public List<Votacion> GetAllVotaciones() {
		return dataAccess.GetAllVotaciones();
	}

	@Override
	public Votacion GetVotacion(int id) {
		return dataAccess.GetVotacion(id);
	}

	@Override
	public void DeleteVotacion(int id) {
		dataAccess.DeleteVotacion(id);
	}

	@Override
	public void ModifyVotacion(Votacion _v) {
		dataAccess.ModifyVotacion(_v);
	}

	@Override
	public Votacion ObtenerVotacionActiva() {
		return dataAccess.ObtenerVotacionActiva();
	}

}
