package service;

import java.util.List;

import entidad.Post;

public interface PostServicio {

	public void AddPost(Post _p);

	public List<Post> GetAllPosts();

	public Post GetPost(int id);

	public void DeletePost(int id);

	public void ModifyPost(Post _p);

}
