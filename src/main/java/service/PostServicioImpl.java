package service;

import java.util.List;

import dao.DaoPost;
import entidad.Post;

public class PostServicioImpl implements PostServicio {

	private DaoPost dataAccess = null;

	public void setDataAccess(DaoPost dataAccess) {
		this.dataAccess = dataAccess;
	}

	@Override
	public void AddPost(Post _p) {
		dataAccess.AddPost(_p);
	}

	@Override
	public List<Post> GetAllPosts() {
		return dataAccess.GetAllPosts();
	}

	@Override
	public Post GetPost(int id) {
		return dataAccess.GetPost(id);
	}

	@Override
	public void DeletePost(int id) {
		dataAccess.DeletePost(id);
	}

	@Override
	public void ModifyPost(Post _p) {
		dataAccess.ModifyPost(_p);
	}

}
