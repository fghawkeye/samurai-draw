package service;

import java.util.List;

import dao.DaoPostEnVotacion;
import entidad.PostEnVotacion;

public class PostEnVotacionServicioImpl implements PostEnVotacionServicio {

	private DaoPostEnVotacion dataAccess = null;

	public void setDataAccess(DaoPostEnVotacion dataAccess) {
		this.dataAccess = dataAccess;
	}

	@Override
	public void AddPostEnVotacion(PostEnVotacion _p) {
		dataAccess.AddPostEnVotacion(_p);
	}

	@Override
	public List<PostEnVotacion> GetAllPostEnVotacion(int idvotacion) {
		return dataAccess.GetAllPostEnVotacion(idvotacion);
	}

	@Override
	public PostEnVotacion GetPostEnVotacion(int idvotacion, int idpost) {
		return dataAccess.GetPostEnVotacion(idvotacion, idpost);
	}

	@Override
	public void DeletePostEnVotacion(PostEnVotacion _p) {
		dataAccess.DeletePostEnVotacion(_p);
	}

	@Override
	public void ModifyPostEnVotacion(PostEnVotacion _p) {
		dataAccess.ModifyPostEnVotacion(_p);
	}

}
