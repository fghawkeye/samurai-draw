package service;

import java.util.List;

import entidad.Comentario;

public interface ComentarioServicio {
	public void AddComentario(Comentario _c);

	public List<Comentario> GetAllComentarios();

	public Comentario GetComentario(int id);

	public void DeleteComentario(int id);

	public void ModifyComentario(Comentario _c);

	public List<Comentario> GetAllComentariosByPost(int id);

}
