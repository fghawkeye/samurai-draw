package service;

import java.util.List;

import dao.DaoCategoria;
import entidad.Categoria;

public class CategoriaServicioImpl implements CategoriaServicio {
	private DaoCategoria dataAccess = null;

	public void setDataAccess(DaoCategoria dataAccess) {
		this.dataAccess = dataAccess;
	}

	@Override
	public void AddCategoria(Categoria _c) {
		dataAccess.AddCategoria(_c);
	}

	@Override
	public List<Categoria> GetAllCategorias() {
		return dataAccess.GetAllCategorias();
	}

	@Override
	public Categoria GetCategoria(int id) {
		return dataAccess.GetCategoria(id);
	}

	@Override
	public void DeleteCategoria(int id) {
		dataAccess.DeleteCategoria(id);
	}
}
