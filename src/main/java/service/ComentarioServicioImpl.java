package service;

import java.util.List;

import dao.DaoComentario;
import entidad.Comentario;

public class ComentarioServicioImpl implements ComentarioServicio {

	private DaoComentario dataAccess = null;

	public void setDataAccess(DaoComentario dataAccess) {
		this.dataAccess = dataAccess;
	}

	@Override
	public void AddComentario(Comentario _c) {
		dataAccess.AddComentario(_c);
	}

	@Override
	public List<Comentario> GetAllComentarios() {
		return dataAccess.GetAllComentarios();
	}

	@Override
	public Comentario GetComentario(int id) {
		return dataAccess.GetComentario(id);
	}

	@Override
	public void DeleteComentario(int id) {
		dataAccess.DeleteComentario(id);
	}

	@Override
	public void ModifyComentario(Comentario _c) {
		dataAccess.ModifyComentario(_c);
	}

	@Override
	public List<Comentario> GetAllComentariosByPost(int id) {
		return dataAccess.GetAllComentariosByPost(id);
	}

}
