package service;

import java.util.List;

import entidad.Categoria;

public interface CategoriaServicio {

	public void AddCategoria(Categoria _c);

	public List<Categoria> GetAllCategorias();

	public Categoria GetCategoria(int id);

	public void DeleteCategoria(int id);
}
