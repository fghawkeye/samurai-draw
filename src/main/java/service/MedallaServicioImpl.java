package service;

import java.util.List;

import dao.DaoMedalla;
import entidad.Medalla;

public class MedallaServicioImpl implements MedallaServicio {

	private DaoMedalla dataAccess = null;

	public void setDataAccess(DaoMedalla dataAccess) {
		this.dataAccess = dataAccess;
	}

	@Override
	public void AddMedalla(Medalla _m) {
		dataAccess.AddMedalla(_m);
	}

	@Override
	public List<Medalla> GetAllMedallas(int idpost) {
		return dataAccess.GetAllMedallas(idpost);
	}

	@Override
	public Medalla GetMedalla(int numMedalla, int idpost, int idvotacion) {
		return dataAccess.GetMedalla(numMedalla, idpost, idvotacion);
	}

	@Override
	public void DeleteMedalla(Medalla m) {
		dataAccess.DeleteMedalla(m);

	}

	@Override
	public void ModifyMedalla(Medalla _m) {
		dataAccess.ModifyMedalla(_m);
	}
}
