package service;

import java.util.List;

import entidad.Medalla;

public interface MedallaServicio {

	public void AddMedalla(Medalla _m);

	public List<Medalla> GetAllMedallas(int idpost);

	public Medalla GetMedalla(int numMedalla, int idpost, int idvotacion);

	public void DeleteMedalla(Medalla m);

	public void ModifyMedalla(Medalla _m);
}
