package service;

import java.util.List;

import dao.DaoVotosPost;
import entidad.VotosPost;

public class VotosPostServicioImpl implements VotosPostServicio {

	private DaoVotosPost dataAccess = null;

	public void setDataAccess(DaoVotosPost dataAccess) {
		this.dataAccess = dataAccess;
	}

	@Override
	public void AddVotosPost(VotosPost _v) {
		dataAccess.AddVotosPost(_v);
	}

	@Override
	public List<VotosPost> GetAllVotosPost(int idvotacion) {
		return dataAccess.GetAllVotosPost(idvotacion);
	}

	@Override
	public VotosPost GetVotosPost(int idvotacion, int idusuario) {
		return dataAccess.GetVotosPost(idvotacion, idusuario);
	}

	@Override
	public void DeleteVotosPost(VotosPost _v) {
		dataAccess.DeleteVotosPost(_v);
	}

	@Override
	public void ModifyVotosPost(VotosPost _v) {
		dataAccess.ModifyVotosPost(_v);
	}

}
