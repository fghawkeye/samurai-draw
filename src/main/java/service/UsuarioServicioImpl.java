package service;

import java.util.List;

import dao.DaoUsuario;
import entidad.Usuario;

public class UsuarioServicioImpl implements UsuarioServicio {

	private DaoUsuario dataAccess = null;

	public void setDataAccess(DaoUsuario dataAccess) {
		this.dataAccess = dataAccess;
	}

	@Override
	public boolean AddUsuario(Usuario _u) {
		return dataAccess.AddUsuario(_u);
	}

	@Override
	public List<Usuario> GetAllUsuarios() {
		return dataAccess.GetAllUsuarios();
	}

	@Override
	public Usuario GetUsuario(int id) {
		return dataAccess.GetUsuario(id);
	}

	@Override
	public void DeleteUsuario(int id) {
		dataAccess.DeleteUsuario(id);
	}

	@Override
	public void ModifyUsuario(Usuario _u) {
		dataAccess.ModifyUsuario(_u);
	}

	@Override
	public Usuario VerificarLogin(String username, String password) {
		return dataAccess.VerificarLogin(username, password);
	}

}
