package service;

import java.util.List;

import entidad.VotosPost;

public interface VotosPostServicio {

	public void AddVotosPost(VotosPost _v);

	public List<VotosPost> GetAllVotosPost(int idvotacion);

	public VotosPost GetVotosPost(int idvotacion, int idusuario);

	public void DeleteVotosPost(VotosPost _v);

	public void ModifyVotosPost(VotosPost _v);

}
