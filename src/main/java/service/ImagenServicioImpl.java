package service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;

@Component
public class ImagenServicioImpl implements ImagenServicio {

	private String cloud_name = "samuraidraw";
	private String api_key = "681138377531697";
	private String api_secret = "l-aWwZ5yXPEPv3Sa_FiBskX14oA";

	public Map SubirImagen(MultipartFile image, String folder) {
		Cloudinary cloudinary = new Cloudinary(
				ObjectUtils.asMap("cloud_name", cloud_name, "api_key", api_key, "api_secret", api_secret));
		try {
			Map uploadResult = cloudinary.uploader().upload(image.getBytes(),
					ObjectUtils.asMap("folder", folder + "/"));
			return uploadResult;
		} catch (Exception e) {
			e.printStackTrace();
			return new HashMap<String, String>();
		}
	}
}
